/********************************************************************************************************************
*	Author : Tim Weatherwax
*	Program Name : Store Project 5
*	Date : April, 30 2014
*	Purpose : This code is the shell for the project 5. You need to fill in the code where indicated below to complete
*		the project. The code will not compile until you complete the methods for the Item class. These
*		methods must match the exact specifications as per the instructions.
*
*********************************************************************************************************************/
// how to check if good zip?
// completed non extra credit, this is for both extra credits

#include <iostream>
#include <string>
#include <fstream>
#include <iomanip>
using namespace std;

const int MAX_CATALOG_SIZE = 20;

struct CartItem {
	int itemId;
	string itemName;
	double price, weight;
	int qtyPurchased;
};


struct Salestax {
	string zipcode;
	double taxrate;
	double shipRate;
};


int loadZipcodes(Salestax arrayForTaxes[])
{
	int position = 0;
	ifstream inStream;
	inStream.open("zipcode list.txt");

	// if file could not open
	if (inStream.fail())
	{
		cout << "Input file could not be opened" << endl;
		system("pause");
		exit(1);
	}

	while (inStream >> arrayForTaxes[position].zipcode >> arrayForTaxes[position].taxrate >> arrayForTaxes[position].shipRate)
	{
		bool valid = true;
		// test to make sure all data input is valid
		if (arrayForTaxes[position].zipcode.length() != 5)
			valid = false;
		else if (arrayForTaxes[position].taxrate < 0.075 || arrayForTaxes[position].taxrate > 0.090)
			valid = false;
		else if (arrayForTaxes[position].shipRate < 1.45 || arrayForTaxes[position].shipRate > 1.95)
			valid = false;
		if (valid)
			position++;
	}

	// check it test works. works
	/*
	cout << "Total Positions " << position << endl;
	for (int i = 0; i < position; i++)
	{
		cout << "Position: " << i << endl;
		cout << "Zip: " << arrayForTaxes[i].zipcode << " "
			 << "Taxrate: " << arrayForTaxes[i].taxrate << " "
			 << "Shiprate: " << arrayForTaxes[i].shipRate << endl;
	}
	*/
	inStream.close();
	return position;
}


class Item {
private:
	int itemId, inventoryCount;
	double price, weight;
	string name, ranking;

public:
//	INSERT YOUR METHODS FOR ALL OF THE CLASS OPTIONS AS PER THE INSTRUCTIONS IN SAKAI
	Item () // default constructor
	{
		itemId = 0;
		inventoryCount = 0;
		price = 0;
		weight = 0;
		name = "";
		ranking = "";
	}

	void setItem(int id, int invCount, double itPrice, double itWeight, string itName, string itRanking)
	{
		itemId = id;
		inventoryCount = invCount;
		price = itPrice;
		weight = itWeight;
		name = itName;
		ranking = itRanking;
	}
	
	double getWeight()
	{
		return weight;
	}

	string getName()
	{
		return name;
	}

	int getId()
	{
		return itemId;
	}

	double getPrice()
	{
		return price;
	}

	string getRank()
	{
		return ranking;
	}

	int getInventory()
	{
		return inventoryCount;
	}

	void setInventory(int value)
	{								
		inventoryCount = inventoryCount + (value);
		
		if (inventoryCount < 0)
		{
			cout << "No more of this item.";
			inventoryCount = 0;
		}
	}
};




/*
*	This class definition represents all of the available items for sale. It contains two private properties
*	itemList is an array that represents each each for sale. It uses the item class.
*	size = this represents the actual number of items in the catalog based upon what is read into from the data
*	file. Size needs to be less than MAX_CATALOG_SIZE. 
*
*/

class Catalog {
private:
	Item itemList[MAX_CATALOG_SIZE];
	int size;

public:
	Catalog() {
		size = 0;
	}

	/*
	*  This method prints out the contents of each item in the catalog. It can be used for debugging purposes
	*  and does not need to be invoked as part of the final version of the program

	*	Do NOT modify this method. 
	*/
	void printCatalog() {
		for (int i = 0; i < size; i++) {
			cout << "catalog[" << i << "] ID = " <<  itemList[i].getId() 
				<< " name = " << itemList[i].getName() 
				<< " price = " << itemList[i].getPrice() 
				<< " weight = " << itemList[i].getWeight()
				<< " count = " << itemList[i].getInventory()
				<< " ranking = " << itemList[i].getRank()
				<< endl;
		}
	}

	/*
	*	This method will display the catalog to the user. It validates the user input to make sure it is an integer.
	*	It then returns the item ID. If it finds the item, it will update the cart item structure. 
	*	Note - cart item structure is an OUTPUT parameter and is passed by reference.

	*	Do NOT modify this method. It should be called from maintain cart.
	*/

	int displayCatalog(CartItem &cartItem) {
		bool validChoice = false;
		int itemChoice;
		do {
			cout << "Please choose an item from the following list. Enter the Item Number" << endl << endl;
			for (int i = 0; i < size; i++) {
				cout << "Item (" << itemList[i].getId() << ") Name = " << itemList[i].getName() << endl
					<< "Price: " << itemList[i].getPrice() << endl
					<< "Remaining quantity = " << itemList[i].getInventory() << endl
					<< "Rank " << itemList[i].getRank() << endl << endl;
			}
			cout << "Item Selected:";
			cin >> itemChoice;
			cout << endl;
			if (cin.fail() ) {
				cin.clear();
				cin.ignore(5);
			} else {
				for (int i = 0; i < size; i++)
					if (itemList[i].getId() == itemChoice)
						if (itemList[i].getInventory() <= 0)
							cout << "That item is no longer available" << endl;
						else {
							cartItem.itemId = itemList[i].getId();
							cartItem.itemName = itemList[i].getName();
							cartItem.weight = itemList[i].getWeight();
							cartItem.price = itemList[i].getPrice();
							validChoice = true;
						}
			}
		} while (!validChoice);
		return itemChoice;
	}


	/*
	* The setCatalogItem function may not be needed by your application. It will assign specific item values to the
	* next empty spot in the catalog. It uses the size property as the last item in the catalog. It then increments
	* the size value. It assumes that setItem is a valid method in the item class. 
	*/
	void setCatalogItem(int id,  int count, double p, double wt, string n, string r) {
		itemList[size].setItem(id,  count, p, wt, n, r);
		size++;
	}

	/*
	*	This function updates the existing quantity for a specific item. It will loop through all items in
	*	the catalog. If it can't find the item, it returns false. 

	*	Do NOT modify this method. It should be called from maintain cart or from return item.
	*/

	bool setInventoryCount(int id, int qtyChange) {
		bool found = false;
		for (int i = 0; !found && i < size; i++)
			if (id == itemList[i].getId()) {
				itemList[i].setInventory(qtyChange);
				found = true;
			}
		return found;
	}

	/*
	*	This function returns the current quantity for a specific item. It will loop through all items in
	*	the catalog. If it can't find the item, it returns -1 indicating an invalid amount. 
	*
	*	Do NOT modify this method. It should be called from maintain cart or from return item.
	*/

	int getInventoryCount(int id) {
		int count = -1;
		for (int i = 0; count == -1 && i < size; i++)
			if (id == itemList[i].getId()) {
				count = itemList[i].getInventory();
			}
		return count;
	}

	/*
	*	This method will open up a file based on the input fileName parameter. This file contains all of the 
	*	items. It will read in each item from the file and save it to the catalog array. You can use the setItem()
	*	method in the item class to update all of the values. 
	*	For each line read in from the file, it needs to increment the size property.
	*/
	void loadInventory(string fileName) {

		ifstream inStream;
		int itId, invCount;
		double itPrice, itWeight;
		string itName, itRanking;
		
		inStream.open(fileName);

		// if file could not open
		while (inStream.fail())
		{
			cout << "Input file could not be opened. Enter a different file name." << endl;
			cin >> fileName;
		}
			
		while (inStream >> itId >> itName >> itRanking >> itPrice >> itWeight >> invCount)
		{
			itemList[size].setItem(itId, invCount, itPrice, itWeight, itName, itRanking);
			size++;
		}
		
		//test if all item info loads into itemList array. works
		/*
		for (int i = 0; i < size; i++)
		{
			cout << "Id " << itemList[i].getId() << endl
				 << "Name " << itemList[i].getName() << endl
				 << "Rank " << itemList[i].getRank() << endl
				 << "Price " << itemList[i].getPrice() << endl
				 << "Weight " << itemList[i].getWeight() << endl
				 << "Quantity " << itemList[i].getInventory() << endl << endl;
		}
		*/
		inStream.close();
	}
} ;

class Cart 
{
private: 
	CartItem cartItemClassArray[100];
	int cartSize;

public:

	Cart ()
	{
		cartSize = 0;
	}
	
	void maintainCart(Catalog &catalogObj)
	{
		int idWanted, qtyWant;
		char moreItems;


		do
		{
	
		idWanted = catalogObj.displayCatalog(cartItemClassArray[cartSize]);
	
		// checks if id they want exists
		if (idWanted)
		{
			cout << "How many would you like to buy? ";
			cin >> qtyWant;

			//makes sure they want a positive amount of items
			while (qtyWant < 0)
			{
				cout << "Enter a positive amount of items: ";
				cin >> qtyWant;
			}
			// makes sure there are enough items in the catalog inventory to buy
			while (qtyWant > catalogObj.getInventoryCount(idWanted))
			{
				cout << "We only have " <<  catalogObj.getInventoryCount(idWanted) << " of these items left. Enter a lower amount. ";
				cin >> qtyWant;
			}
			
			// sets qty of item to arr
			cartItemClassArray[cartSize].qtyPurchased = qtyWant;
		
			//sets the amount of the item to how many are left in stock
			catalogObj.setInventoryCount(idWanted, -qtyWant);

			cartSize++;

			cout << "Would you like keep buying? ";
			cin >> moreItems;
		}
		} while (moreItems == 'y' || moreItems == 'Y');
	system("cls");
	} 

	void printReceipt(double sub, double shipCost, double tax, double tot, string name)
	{
		ofstream outStream;
		outStream.open("Receipt.html");

		cout.setf(ios::fixed);
		cout.setf(ios::showpoint);
		cout.precision(2);

		outStream << "<!DOCTYPE html>" << endl 
				  << "<html>" << endl
				  << "<head>" << endl
				  << "<style>" << endl
				  << "th" << endl
				  << "{" << endl
				  << "text-align:left;" << endl
				  << "}" << endl
				  << "</style>" << endl
				  << "</head>" << endl
				  << "<body>" << endl
				  << "<h3>Receipt for " << name << ".</h3><hr>" << endl << endl << endl
				  << "<table style=\"width:900px\">" << endl
				  << "<tr>" << endl
				  << "<th>Item:</th>" << endl
				  << "<th>Price:</th>" << endl
				  << "<th>Quantity:</th>" << endl << endl << endl;

		for (int i = 0; i < cartSize; i++)
		{
			if (cartItemClassArray[i].qtyPurchased != 0)
			{
			outStream << "<tr>" << endl 
					  << "<td>" << cartItemClassArray[i].itemName << "</td>" << endl
					  << "<td>$" << cartItemClassArray[i].price << "</td>" << endl 
					  << "<td>(" << cartItemClassArray[i].qtyPurchased << ")</td>" << endl
					  << "</tr>" << endl;
			}
		}
		outStream << "</table>" << endl
				  << "<br><br><hr>" << endl;

		outStream << "<p>Subtotal: $" << sub << "</p>" << endl
				  << "<p>Tax: $" << tax << "</p>" << endl
				  << "<p>Shipping: $" << shipCost << "</p>" << endl
				  << "<p>Total: $" << tot << "</p>" << endl << endl;

		outStream << "<hr>" << endl
				  << "<p>Thanks for shopping!</p>" << endl
				  << "</body>" << endl
				  << "</html>" << endl;



		outStream.close();
	}

	void returnItem(Catalog &catalogObj)
	{
		int itemReturn, qtyReturn, idReturn;

		if (cartSize == 0)
		{
			cout << "Cart is empty." << endl;
		}
		else
		{
		cout << "Which item number would you like to return?" << endl << endl;

		for (int i = 0; i < cartSize; i++)
		{
			cout << i << ") " << cartItemClassArray[i].itemName << " Quantity: " << cartItemClassArray[i].qtyPurchased << endl;
		}

		cin >> itemReturn;

		while(itemReturn < 0 || itemReturn > cartSize)
		{
			cout << "Enter a valid item to return" << endl;
			cin >> itemReturn;
		}

		cout << "How many? " << endl;
		cin >> qtyReturn;
		
		// add back into catalog and out of cart
		for (int i = 0; i < cartSize; i++)
		{
			if (itemReturn == i)
			{
				while (qtyReturn < 0 || qtyReturn > cartItemClassArray[i].qtyPurchased)
				{
					cout << "Enter a valid amount to return. ";
					cin >> qtyReturn;
				}
		
				idReturn = cartItemClassArray[i].itemId;
				cartItemClassArray[i].qtyPurchased = cartItemClassArray[i].qtyPurchased - qtyReturn;			
				/*
				if (cartItemClassArray[i].qtyPurchased == 0)
				{
					for (int j = i; j < cartSize; j++)
					{
						cartItemClassArray[j].itemId = cartItemClassArray[cartSize].itemId;
						cartItemClassArray[j].itemName = cartItemClassArray[cartSize].itemName;
					}	

					cartSize--;
				}
				*/
			}
		}
		
		catalogObj.setInventoryCount(idReturn, qtyReturn);
		/*
		for (int i = 0; i < cartSize; i++)
		{
			if (cartItemClassArray[i].qtyPurchased == 0)
				{
					for (int j = i; j < cartSize - 1; j++)
					{
						cartItemClassArray[j].itemId = cartItemClassArray[i+1].itemId;
					}	

					cartSize--;
				}
		}
		*/
		// test if item returned.works
		/*
		for (int i = 0; i < cartSize; i++)
		{
			cout << "Item " << i << " " << cartItemClassArray[i].itemName << " Amount " << cartItemClassArray[i].qtyPurchased << endl;
		}
		*/
		
		system("cls");
		}
	}

	void checkout(Salestax taxArray[], int numOfZips)
	{
		double shipmentCost, taxes, subtotal, total;
		string zipcode, customerName;

		cout.setf(ios::fixed);
		cout.setf(ios::showpoint);
		cout.precision(2);

		cout << "What is your first name? ";
		cin >> customerName;

		cout << "Enter your zipcode: ";
		cin >> zipcode;		

		while (zipcode.length() != 5)
		{
			cout << "Enter a valid zipcode.";
			cin >> zipcode;
		}
		
		while (cin.fail())
		{
			cout << "Enter a valid zip";
			cin >> zipcode;
		}
	
		subtotal = calculateSubtotal();
		shipmentCost = calculateShipping(taxArray, zipcode, numOfZips);	
		taxes = calculateTax(taxArray, numOfZips, zipcode, subtotal);
		total = calculateTotal(subtotal, shipmentCost, taxes);

		system("cls");
		cout << "Thanks for shopping!" << endl
			 << "Your receipt has also been output in a html file." << endl;
		printReceipt(subtotal, shipmentCost, taxes, total, customerName);
		displayReceipt(subtotal, shipmentCost, taxes, total);

		
	}

	int displayMenu()
	{
		int menuOption;

		cout << "Choose a number option. " << endl << endl
			 << "1) Shop " << endl
			 << "2) Return Item " << endl
			 << "3) Checkout " << endl
			 << "4) Empty Cart " << endl
			 << "5) Exit Store (without buying) " << endl << endl;
		
		
		cin >> menuOption;

		while (menuOption < 1 || menuOption > 5)
		{
			cout << "Choose a valid option";
			cin >> menuOption;
		}

		//system("cls");

		return menuOption;
	}

	void shop(Salestax taxArray[], int numOfZipcodes, Catalog &catalogObj)
	{
		int choice;

		do
		{
		
		choice = displayMenu();

		if (choice == 1) // shop
		{
			maintainCart(catalogObj);
		}
		else if (choice == 2) // return item
		{
			returnItem(catalogObj);
		}
		else if (choice == 3) // checkout
		{
			checkout(taxArray, numOfZipcodes);
			break;
		}
		else if (choice == 4) // empty cart
		{
			cartSize = 0;
			cout << endl;
		}
		
		} while (choice != 5);


	}

	double calculateSubtotal()
	{
		double subtotal = 0;

		for (int i = 0; i < cartSize; i++)
		{
			if (cartItemClassArray[i].qtyPurchased != 0)
			{
				subtotal = subtotal + (cartItemClassArray[i].price * cartItemClassArray[i].qtyPurchased);
			}
			
		}

		return subtotal;
	}

	double calculateShipping (Salestax shipRatesArr[], string zipcode, int totalZips)
	{
		double rate = 0, weight = 0, shipRate = 0;
		//bool zipMatch;
		/* 
		//badtest.
		do
		{
		for (int i = 0; i < totalZips; i++)
		{
			if(zipcode == shipRatesArr[i].zipcode)
			{
				zipMatch = true;
			}
		}

		if (zipMatch == false)
		{
			cout << "Enter a different zipcode ";
			cin >> zipcode;
		}
		} while (zipMatch == false);

		*/

		for (int i = 0; i < totalZips; i++)
		{
			if (zipcode == shipRatesArr[i].zipcode)
			{
				shipRate = shipRatesArr[i].shipRate;
			}
		}

		for (int i = 0; i < cartSize; i++)
		{
			if (cartItemClassArray[i].qtyPurchased != 0)
			{
				weight = weight + (cartItemClassArray[i].qtyPurchased * cartItemClassArray[i].weight);
			}		
		}
		rate = weight * shipRate;

		return rate;
	}

	double calculateTax(Salestax shipRatesArr[], int totalShipPossible, string zip, double subtotal)
	{
		double taxRate = 0, totalTax;

		for (int i = 0; i < totalShipPossible; i++)
		{
			if (zip == shipRatesArr[i].zipcode)
			{
				taxRate = shipRatesArr[i].taxrate;
			}
		}


		totalTax = taxRate * subtotal;

		return totalTax;
	}

	double calculateTotal(double sub, double shipping, double taxrate)
	{
		double total;

		total = sub + shipping + taxrate;
		return total;
	}

	void displayReceipt(double subtotal, double shipping, double taxes, double total)
	{

		cout << "Receipt: " << endl << endl;

		for (int i = 0; i < cartSize; i++)
		{
			if (cartItemClassArray[i].qtyPurchased != 0)
			{
			cout << "Item: " << cartItemClassArray[i].itemName << " Quantity: " << cartItemClassArray[i].qtyPurchased << " Price: " << cartItemClassArray[i].price << endl;
			}
		}

		cout << "Subtotal: " << subtotal << endl
			 << "Shipping: " << shipping << endl
			 << "Tax: " << taxes << endl
			 << "Total: " << total << endl << endl;

	}



};


//	OTHER STORE FUNCTIONS GO HERE - MAINTAINCART, CALCULATETAX, ETC.

/*
*	Modify your main program as needed to use the catalog definitions above.
*/

int main() {
	Salestax taxArray[3000];
	Catalog catalog;
	int numOfZips;
	Cart cart;

	cout.setf(ios::fixed);
	cout.setf(ios::showpoint);
	cout.precision(2);
	
	numOfZips = loadZipcodes(taxArray);	

	catalog.loadInventory("HotTub Catalog.txt");
	
	cout << "Welcome to the Hot Tub Store!" << endl;

	cart.shop(taxArray, numOfZips, catalog);

	system("pause");
}