// .h for VGanimal
// tim weatherwax

#ifndef VGANIMAL_H
#define VGANIMAL_H

#include "chooseAnimal.h"
#include <vector>

class VGanimal : public ChooseAnimal
{
	public:
		VGanimal(char symbol);

		int visit(Player &p, vector<string>& v);
		void draw();
}; //Do NOT forget this semicolon
#endif