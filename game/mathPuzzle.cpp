// mathPuzzle.cpp inherited from location
// solve a math puzzle at a location
// tim weatherwax

// how to make it when they lose?

#include <iostream>
#include <string>
#include "location.h"
#include "mathPuzzle.h"
#include "videoGame.h"
#include <Windows.h>
#include <vector>

using namespace std;

// default constructor
// passes blank to Location if nothing passed in
MathPuzzle::MathPuzzle(char s):Location(s)
{
	solved = false; // puzzle starts out unsolved
}

// virtual from location class, definition is different from definition in location class
// always need to draw something after been to this location and done something
void MathPuzzle::draw()
{
	/*
	if (visited == true && solved == false)
		cout << symbol;
	else if (solved == true)
		cout << " ";
	else
		cout << ".";
		*/
	;
}

// virtual from location class, definition is different from definition in location class
// this is invoked once you visit a certain location on map, solves a puzzle
int MathPuzzle::visit(Player &p, vector<string>& v)
{
	//int tries = 0;
	//char answer;

	if(visited == false)
	{
		visited = true;

		//Videogame::setMessage2("Do you like this game?");
		//Videogame::setMessage3("Yes or no?");
		
		string message = "Do you like this game? (y/n)";
		string title = "";

		int answer = MessageBoxA(NULL, message.c_str(), title.c_str(), MB_YESNO);

		if (answer == IDYES) //NOTE: You can also compare against IDYES to see if the the user selects the "YES" button
		{
			return 1;
		}
		else
			return 10;

		/*
		if(yes)
			return 1;
		else
			return 10;
			*/

		/*

		while(solved == false && tries < 3) // then try to solve a puzzle
		{
			cout << "Answer the riddle:" << endl
				 << "Why is 6 afraid of 7?" << endl << endl
				 << "1) 6 is weak" << endl 
				 << "2) Because 7, 8, 9" << endl
				 << "3) Terrible question" << endl
				 << "4) Who cares" << endl;
			
			cin >> answer;
			cout << endl;

			while(answer != '1' && answer != '2' && answer != '3' && answer != '4')
			{
				cout << "Enter a valid option" << endl;
				cin >> answer;
				cout << endl;
			}

			if(answer == '1')
			{
				cout << "Try again" << endl;
				tries++;
				cout << "Tries left: " << 3 - tries << endl << endl;
			}
			else if(answer == '2')
			{
				cout << "Correct answer!" << endl << endl;
				solved = true;
				return 1;
			}
			else if(answer == '3')
			{
				cout << "Nope. Try again." << endl;
				tries++;
				cout << "Tries left: " << 3 - tries << endl << endl;
			}
			else 
			{
				cout << "Someone somewhere cares. Try again." << endl;
				tries++;
				cout << "Tries left: " << 3 - tries << endl << endl;
			}
				
		}
		
		if(tries == 3)
		{
			//cout << "Sorry you loose." << endl;
			return 10;
		}
		*/
	}
	return 2;
}