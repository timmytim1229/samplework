// chooseAnimal.h inherited from location
// user chooses an animal at a location
// tim weatherwax

#include <iostream>
#include <string>
#include "location.h"
#include "player.h"
#include "chooseAnimal.h"

using namespace std;

// default constructor
// passes blank to Location if nothing passed in, sets animal to blank
ChooseAnimal::ChooseAnimal(char s):Location(s)
{
	caught = false;
	/*
	Item *sp = new Item("Sphinx"); // for item you want to pick up from ground
	ground.insert(sp);
	Item *c = new Item("Condor"); // for item you want to pick up from ground
	ground.insert(c);
	Item *h = new Item("Hydra"); // for item you want to pick up from ground
	ground.insert(h);
	*/
}

// virtual from location class, definition is different from definition in location class
// always need to draw something after been to this location and done something
void ChooseAnimal::draw()
{
	if (visited == true && caught == false)
		cout << symbol;
	else if (caught == true)
		cout << " ";
	else
		cout << ".";
}

// virtual from location class, definition is different from definition in location class
// this is invoked once you visit a certain location on map, user chooses an animal
int ChooseAnimal::visit(Player &p)
{
	char choice;

	if(visited == false)
	{
		visited = true;

		cout << "Three animals crossed your path! Which one do you want to capture?" << endl
			 << "1) Sphinx " << endl
			 << "2) Condor " << endl
			 << "3) Hydra " << endl 
			 << "4) None " << endl << endl;
		cin >> choice;
		cout << endl;

		while(choice != '1' && choice != '2' && choice != '3' && choice != '4')
		{
			cout << "Choose a valid option" << endl;
			cin >> choice;
		}

		if(choice == '1') // sphinx
		{
			cout << "Congratulations! You captured a Sphinx!" << endl << endl
				 << "Sphinx: A sphinx is a mythical creature with, as a minimum, " << endl
				 << "the body of a lion and a human head. " << endl
				 << "In Greek tradition, it has the haunches of a lion, " << endl
				 << "sometimes with the wings of a great bird, and the face of a human. " << endl << endl;
			/*
			//cout << "Trying to remove" << endl;
			Item *i = ground.remove("Sphinx"); // removes from ground
			//cout << "Removed" << endl;
			if(i != 0) // if did remove crystal from the list
			{
				p.pickUp(i); // adds to inventory. this will put the item that i points to in the players inventory
			}
			*/
			//p.setAnimal("Sphinx");
			caught = true;
			return 1;
		}
		else if(choice == '2') // condor
		{
			cout << "Congratulations! You captured a Condor!" << endl << endl
				 << "Condor: A large black vulture with a ruff of white feathers " << endl
				 << "with a wingspan of up to 3.2 m/10.5 ft. " << endl
				 << "It is a scavenger, feeding on carrion. It prefers large carcasses " << endl
				 << "and sometimes medium sized animals when needed." << endl << endl;
			/*
			Item *i = ground.remove("Condor"); // removes from ground
			
			if(i != 0) // if did remove crystal from the list
			{
				p.pickUp(i); // adds to inventory. this will put the item that i points to in the players inventory
			}
			*/
			//p.setAnimal("Condor");
			caught = true;
			return 1;
		}
		else if(choice == '3') // hydra
		{
			cout << "Congratulations! You captured a Hydra!" << endl << endl
				 << "Hydra: An ancient serpent-like water monster " << endl
				 << "with reptilian traits. It possesses many heads " << endl
				 << "that grow back twice as many when one is cut off. " << endl
				 << "It has poisonous breath and poisionous blood. " << endl << endl;
			/*
			Item *i = ground.remove("Hydra"); // removes from ground
			
			if(i != 0) // if did remove crystal from the list
			{
				p.pickUp(i); // adds to inventory. this will put the item that i points to in the players inventory
			}
			*/
			//p.setAnimal("Hydra");
			caught = true;
			return 1;
		}
		else // no aminal
		{
			return 2;
		}
	}
	else
		return 2;
}