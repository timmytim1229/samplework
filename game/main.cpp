//FileName:		main.cpp
//Programmer:	Dan Cliburn, Tim Weatherwax
//Date:			6/13/2014
//Purpose:		This file defines the main function for this simple video game 

/*****************************************************************************
*							Added Features:
*
* 1) sound on getting crystal
* 2) added a vector that is used as an inventory, displays at end of game 
* 3) entire game is timed and lose if time is up
*****************************************************************************/

#include "videogame.h"

int main(int argc, char *argv[])  //main must take these parameters when using SDL
{
	Videogame g;

	g.playGame();

	return 0;
}

