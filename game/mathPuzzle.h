// mathPuzzle.h inherited from location
// solve a math puzzle at a location
// tim weatherwax

#include <iostream>
#include <string>
#include <vector>
#include "location.h"

using namespace std;

#ifndef MATHPUZZLE_H
#define MATHPUZZLE_H

class MathPuzzle : public Location
{
private:
	bool solved;

public:
	// default constructor
	// passes blank to Location if nothing passed in
	MathPuzzle(char s = ' ');

	// virtual from location class, definition is different from definition in location class
	// always need to draw something after been to this location and done something
	void draw();
	
	// virtual from location class, definition is different from definition in location class
	// this is invoked once you visit a certain location on map, solves a puzzle
	int visit(Player &p, vector<string>& v);

};

#endif