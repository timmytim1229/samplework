// player.h header for player prototypes
// tim weatherwax

#include <iostream>
#include <string>

using namespace std;

#ifndef PLAYER_H
#define PLAYER_H

class Player
{
private:
	int age;
	string name;
	string animal;
	string crystal;
	string item;
	//LinkedList inventory; // to have player have inventory. the items are in theoretically in any location cuz player can drop items anywhere
	//queue <Item> q;

public:
	// default constructor
	Player ();

	Player (string it);

	// age accessor
	int getAge();

	// name accessor
	string getName();

	// animal accessor
	string getAnimal();

	string getCrystal();

	void setCrystal(string cryst);

	string getItem();

	void setItem(string itm);

	// age mutator
	void setAge(int playerAge);

	// name mutator
	void setName(string playerName);

	// animal mutator
	void setAnimal(string ani);

	//prints info of players
	void printInfo();

	/*
	void pickUp(Item *i);

	Item *drop(string name);

	// prints whats in inventory
	void printInventory(); 

	// checks to see if have all conditions to end the game
	bool hasAllItems();
	*/
};

ostream& operator << (ostream& out, Player s);

#endif