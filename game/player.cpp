// player class methods
// tim weatherwax

#include <iostream>
#include <string>
#include <iomanip>
#include "player.h"

using namespace std;

Player::Player()
{
	age = 0;
	name = "";
	animal = "";
	crystal = "";
}

Player::Player(string it)
{
	item = it;
}

// age accessor
int Player::getAge()
{
	return age;
}

// name accessor
string Player::getName()
{
	return name;
}

// animal accessor
string Player::getAnimal()
{
	return animal;
}

// age mutator
void Player::setAge(int playerAge)
{
	age = playerAge;
}

// name mutator
void Player::setName(string playerName)
{
	name = playerName;
}

// animal mutator
void Player::setAnimal(string ani)
{
	animal = ani;
}

string Player::getCrystal()
{
	return crystal;
}

void Player::setCrystal(string cryst)
{
	crystal = cryst;
}

string Player::getItem()
{
	return item;
}

void Player::setItem(string itm)
{
	item = itm;
}

//prints info of players
void Player::printInfo()
{
	cout << "Name: " << name << endl
		 << "Age: " << age << endl << endl;
}

ostream& operator << (ostream& out, Player p)
{
	out.setf(ios::fixed);
	out.precision(2);
	out << left << setw(10) << p.getItem() << endl;
	return out;
}

/*
// puts item in queue
void Player::pickUp(Item *i) // after already taken from ground
{
	inventory.insert(i);
}

// takes item out of inventory
Item *Player::drop(string name)
{
	Item *i;
	i = inventory.remove(name); // gives back pointer to item
	return i; // returns the pointer to game

}

void Player::printInventory()
{
	Item *i = inventory.getFirstElement(); 

	cout << "Inventory: " << endl;
	while(i != 0)
	{
		cout << i->getName() << endl;
		i = i->getNext();
	}

}

bool Player::hasAllItems()
{
	Item *cry;
	Item *anim, *anim2, *anim3;
	int store = 0;
	int animCtr = 0;

	cry = inventory.find("crystal");
	anim = inventory.find("Condor");
	anim2 = inventory.find("Sphinx");
	anim3 = inventory.find("Hydra");
	
	if(cry != 0)
	{
		store++;
	}

	if(anim != 0 || anim2 != 0 || anim3 != 0)
	{
		animCtr++;
	}

	if(store == 1 && animCtr >= 1)
	{
		return true;
	}

	return false;
}

*/