// .cpp for VGanimal
// tim weatherwax

#include "VGanimal.h"
#include "player.h"
#include "videoGame.h"
#include "chooseAnimal.h"
#include <vector>

VGanimal::VGanimal(char symbol) : ChooseAnimal(symbol)
{				
	;
}

void VGanimal::draw()
{
	if (visited == true)
	{
		Videogame::renderBitMap( ((int)(symbol)) );
			//This code converts the symbol (a char) to an integer
			//to be used as the texture ID for this type of object
	}
}

int VGanimal::visit(Player &p, vector<string>& v)
{
	//char choice;

	if (visited == false)
	{
		Videogame::setMessage2("Congratulations! You captured a Condor!");
		
		v.push_back("Condor");
		p.setAnimal("Condor");


		visited = true;
		return 1;

		/*
		visited = true;

		cout << "Three animals crossed your path! Which one do you want to capture?" << endl
			 << "1) Sphinx " << endl
			 << "2) Condor " << endl
			 << "3) Hydra " << endl 
			 << "4) None " << endl << endl;
		cin >> choice;
		cout << endl;

		while(choice != '1' && choice != '2' && choice != '3' && choice != '4')
		{
			cout << "Choose a valid option" << endl;
			cin >> choice;
		}

		if(choice == '1') // sphinx
		{
			cout << "Congratulations! You captured a Sphinx!" << endl << endl
				 << "Sphinx: A sphinx is a mythical creature with, as a minimum, " << endl
				 << "the body of a lion and a human head. " << endl
				 << "In Greek tradition, it has the haunches of a lion, " << endl
				 << "sometimes with the wings of a great bird, and the face of a human. " << endl << endl;
			/*
			//cout << "Trying to remove" << endl;
			Item *i = ground.remove("Sphinx"); // removes from ground
			//cout << "Removed" << endl;
			if(i != 0) // if did remove crystal from the list
			{
				p.pickUp(i); // adds to inventory. this will put the item that i points to in the players inventory
			}
			*/
		/*
			//p.setAnimal("Sphinx");
			caught = true;
			return 1;
		}
		else if(choice == '2') // condor
		{
			cout << "Congratulations! You captured a Condor!" << endl << endl
				 << "Condor: A large black vulture with a ruff of white feathers " << endl
				 << "with a wingspan of up to 3.2 m/10.5 ft. " << endl
				 << "It is a scavenger, feeding on carrion. It prefers large carcasses " << endl
				 << "and sometimes medium sized animals when needed." << endl << endl;
			/*
			Item *i = ground.remove("Condor"); // removes from ground
			
			if(i != 0) // if did remove crystal from the list
			{
				p.pickUp(i); // adds to inventory. this will put the item that i points to in the players inventory
			}
			*/
		/*
			//p.setAnimal("Condor");
			caught = true;
			return 1;
		}
		else if(choice == '3') // hydra
		{
			cout << "Congratulations! You captured a Hydra!" << endl << endl
				 << "Hydra: An ancient serpent-like water monster " << endl
				 << "with reptilian traits. It possesses many heads " << endl
				 << "that grow back twice as many when one is cut off. " << endl
				 << "It has poisonous breath and poisionous blood. " << endl << endl;
			/*
			Item *i = ground.remove("Hydra"); // removes from ground
			
			if(i != 0) // if did remove crystal from the list
			{
				p.pickUp(i); // adds to inventory. this will put the item that i points to in the players inventory
			}
			*/
		/*
			//p.setAnimal("Hydra");
			caught = true;
			return 1;
		}
		else // no aminal
		{
			return 2;
		}
	}
	else
		return 2;
		
		*/


	}
	//visited = true;
	return 2;
}