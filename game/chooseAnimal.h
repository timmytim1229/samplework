// chooseAnimal.h inherited from location
// user chooses an animal at a location
// tim weatherwax

#include <iostream>
#include <string>
#include "location.h"
#include "player.h"

using namespace std;

#ifndef CHOOSEANIMAL_H
#define CHOOSEANIMAL_H

class ChooseAnimal : public Location
{
protected:
	bool caught;

public:
	// default constructor
	// passes blank to Location if nothing passed in, sets animal to blank
	ChooseAnimal(char s = 'a');

	// virtual from location class, definition is different from definition in location class
	// always need to draw something after been to this location and done something
	virtual void draw();
	
	// virtual from location class, definition is different from definition in location class
	// this is invoked once you visit a certain location on map, user chooses an animal
	virtual int visit(Player &p);

};

#endif