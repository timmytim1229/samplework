// Timothy Weatherwax t_weatherwax@u.pacific.edu

#ifndef MAIN_H
#define MAIN_H

struct clue
{
	char dir;
	char wordAnswer[256], aClue[256];
  	int found, startRow, startCol;
};

// Function definitions
int getCrossWordFile(struct clue *cluess, int *numOfRows, int *numOfCols); // gets crossword input file
void printClues(struct clue * cluess, int numClues);

#endif
