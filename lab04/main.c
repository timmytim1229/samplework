// Timothy Weatherwax t_weatherwax@u.pacific.edu

#include <stdlib.h>
#include <stdio.h>
#include <string.h>
#include <strings.h>
#include <ctype.h>
#include "main.h"
#include "memory.h"

int main(void)
{
  struct clue clues[100];
  int amtClues, numRows, numCols, wins = 0, setUp = 0;
  
  printf("\nWelcome to the Crossword Puzzle!\n\n"); 
  
  amtClues = getCrossWordFile(clues, &numRows, &numCols); // inputs crossword puzzle
  
  char **board = createArray(numRows, numCols); 
  
  // in main, no * to print numRows, numCols
 //printf("Main: Number of Clues: %i Rows = %d Cols = %d \n\n", amtClues, numRows, numCols);
  
  fillArray(board, numRows, numCols, clues, amtClues, setUp);
  printArray(board, numRows, numCols);
  printClues(clues, amtClues);
  
  setUp = 1;
  
  for(int i = 0; i < amtClues; i++)
  {
  	clues[i].found = 0;
  }
  
  //plays the game

  while(wins < amtClues)
  {
    int choice;
    //int counter = 0;
    char solution[256];
  	
  	printf("Enter your number clue to solve or -1 to quit\n");
  	scanf("%i", &choice); // need & for everything except arrays when use scanf
  	
  	//printf("Correct Answers out of %i: %i\n\n", amtClues, wins);
  	
  	if(choice == -1)
  	{
  		wins = amtClues + 1;
  	}
  	
  	// add a check to make sure they input a number from 1 to amtClues
  	while(choice > amtClues || choice < 1)
  	{
  		if(choice == -1)
  		{
  			break;
  		}
  		
  		printf("Please enter an ANSWER then a NUMBER bewteen 1 and %i.\n\n", amtClues);
  		scanf("%s",solution);
  		scanf("%i",&choice);
  		
  		if(choice == -1)
  		{
  			break;
  		}
  		
  		//counter++;
  		/*
  		if(counter == 5)
  		{
  			printf("SET DEFAULT CHOICE TO 1\n\n");
  			choice = 1;
  		}
  		*/
  	}
  	
  	if(choice == -1)
  	{
  		wins = amtClues + 1;
  	}
  	
  	//printf("IN WHILE LOOP\n\n");
  	
  	for(int i = 1; i <= amtClues; i++)
  	{
  	//printf("IN FOR LOOP %i TIMES\n\n", i);
  	//printf("%i CLUES\n\n", amtClues);
  		if(choice == i && clues[i - 1].found == 0)
  		{
  			printf("Enter your solution:\n");
  			scanf("%s", solution);
  		
  			if (strcasecmp (solution, clues[i - 1].wordAnswer) == 0) 
  			{
  				clues[i - 1].found = 1;
		    	fillArray(board, numRows, numCols, clues, amtClues, setUp);
		    	printf("Correct answer.\n\n");
		    	wins++;
        	}
        	else
        	{
        		printf("Sorry, wrong answer.\n\n");
        	}
   		}
  	}
  	
  	/*
  	if(choice == 1 && clues[choice - 1].found == 0)
  	{
  		printf("Enter your solution:\n");
  		scanf("%s", solution);
  		
  		if (strcasecmp (solution, clues[choice - 1].wordAnswer) == 0) 
  		{
  			clues[choice - 1].found = 1;
        	fillArray(board, numRows, numCols, clues, amtClues, setUp);
        	printf("Correct answer.\n\n");
        	wins++;
        }
        else
        {
        	printf("Sorry, wrong answer.\n\n");
        }
    }
  	else if(choice == 2 && clues[choice - 1].found == 0)
  	{
  		printf("Enter your solution:\n");
  		scanf("%s", solution);
  		
  		if (strcasecmp (solution, clues[choice - 1].wordAnswer) == 0) 
  		{
  			clues[choice - 1].found = 1;
        	fillArray(board, numRows, numCols, clues, amtClues, setUp);
        	printf("Correct answer.\n\n");
        	wins++;
        }
        else
        {
        	printf("Sorry, wrong answer.\n\n");
        }
  	}
  	else if(choice == 3 && clues[choice - 1].found == 0)
  	{
  		printf("Enter your solution:\n");
  		scanf("%s", solution);
  		
  		if (strcasecmp (solution, clues[choice - 1].wordAnswer) == 0) 
  		{
  			clues[choice - 1].found = 1;
        	fillArray(board, numRows, numCols, clues, amtClues, setUp);
        	printf("Correct answer.\n\n");
        	wins++;
        }
        else
        {
        	printf("Sorry, wrong answer.\n\n");
        }
  	}
  	else if(choice == 4 && clues[choice - 1].found == 0)
  	{
  		printf("Enter your solution:\n");
  		scanf("%s", solution);
  		
  		if (strcasecmp (solution, clues[choice - 1].wordAnswer) == 0) 
  		{
  			clues[choice - 1].found = 1;
        	fillArray(board, numRows, numCols, clues, amtClues, setUp);
        	printf("Correct answer.\n\n");
        	wins++;
        }
        else
        {
        	printf("Sorry, wrong answer.\n\n");
        }
  	}
  	else if(choice == 5 && clues[choice - 1].found == 0)
  	{
  		printf("Enter your solution:\n");
  		scanf("%s", solution);
  		
  		if (strcasecmp (solution, clues[choice - 1].wordAnswer) == 0) 
  		{
  			clues[choice - 1].found = 1;
        	fillArray(board, numRows, numCols, clues, amtClues, setUp);
        	printf("Correct answer.\n\n");
        	wins++;
        }
        else
        {
        	printf("Sorry, wrong answer.\n\n");
        }
  	}
  	else if(choice == 6 && clues[choice - 1].found == 0)
  	{
  		printf("Enter your solution:\n");
  		scanf("%s", solution);
  		
  		if (strcasecmp (solution, clues[choice - 1].wordAnswer) == 0) 
  		{
  			clues[choice - 1].found = 1;
        	fillArray(board, numRows, numCols, clues, amtClues, setUp);
        	printf("Correct answer.\n\n");
        	wins++;
        }
        else
        {
        	printf("Sorry, wrong answer.\n\n");
        }
  	}
  	else if(choice == -1)
  	{
  		wins = 7;
  	}
  	*/
  	
  	printArray(board, numRows, numCols);
    printClues(clues, amtClues);
  	
  };

  
  
  printf("\nGood Game!\n\n");
  
  return 0;
}

int getCrossWordFile(struct clue * cluess, int *numOfRows, int *numOfCols)
{
  char dir;
  int row, col, theClues, rows, cols;
  // arrays for a string must be at least one character longer than any given word to account for the new line character
  char answer[256], description[256], fileName[256];
  FILE *fp; // must be a pointer of type FILE
  char *mode = "r"; // used to 'r' or read what is in a file
  
  //open file	
  printf("Enter the file you want to open. \n1. crossword_puzzle.txt \n2. crossword_puzzle_test.txt\n");
  /*
  scanf("%i",&fileNumber);
  
  while(fileName != 1 && fileName != 2)
  {
  	printf("Enter a number 1 or 2");
  	scanf("%i",&fileNumber);
  }
  */
  scanf("%s", fileName);
 
  fp = fopen(fileName, mode);
  
  printf("\n\n");
  
  if(fp == NULL)
  {
    fprintf(stderr, "Cannot open file.\n");
    exit(1);
  }
 
  // gets first line
  fscanf(fp, "%i %i %i ", &rows, &cols, &theClues);
  *numOfRows = rows;
  *numOfCols = cols;
  
  
  // makes the blank instance of the array for board, use * to pass the values
  //char **board = createArray(*numOfRows, *numOfCols); 
  //**board = createArray(*numOfRows, *numOfCols); 
  
  // in the function, need * to print the values of the local variables numOfRows, numOfCols
  //printf("getCrosswordFile: Rows = %d Cols = %d  Clues = %i \n", *numOfRows, *numOfCols, theClues);
  
  // continues from where fscanf left off, tests to see if file was imported correctly
  for(int i = 0; i < theClues; i++)
  {
    fscanf(fp,"%c %i %i %255s %255[^\n] ", &dir, &row, &col, answer, description);
       
    // imports to the clue struct
    cluess[i].dir = dir;
    cluess[i].startRow = row;
    cluess[i].startCol = col;
    strcpy(cluess[i].wordAnswer, answer);
    strcpy(cluess[i].aClue, description);
    
    /*
    printf("Completed %i i/o \n\n", i);

    printf("Position (%i) Direction = %c Row = %i Col = %i Answer = %s  Desc = %s \n", i, cluess[i].dir, cluess[i].startRow, cluess[i].startCol, cluess[i].wordAnswer, cluess[i].aClue);
    */
      
  }
  //fillArray(board, numOfRows, numOfCols, cluess, theClues);
  //printArray(board, numOfRows, numOfCols);
  return theClues;
}

void printClues(struct clue * cluess, int numClues)
{
  int position;
  
  printf("\nUnsolved Clues:\n");
  printf("# \t Direction: \t Row/Col: \t Clue: \n");
  
  for (int i = 0; i < numClues; i++) 
  {
    if(cluess[i].found != 1)
    {
    	position = i + 1;
    	
    	printf("%i \t %c \t\t %i/%i \t\t %s \n", position, cluess[i].dir, cluess[i].startRow, cluess[i].startCol, cluess[i].aClue);
    }
  }


return;
}


