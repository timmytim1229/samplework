// Timothy Weatherwax t_weatherwax@u.pacific.edu

#include <stdio.h>   // Allows printf, ...
#include <string.h>
#include <stdlib.h>  // Allows malloc, ...
#include <errno.h>   // Allows errno
#include <ctype.h>
#include "main.h"
#include "memory.h"



char** createArray(int rows, int cols) {
  char **myArray;
  
  // Allocate a 1xROWS array to hold pointers to more arrays (because a string is an 	  array of chars)
  myArray = calloc(rows, sizeof(int *));
  if (myArray == NULL) {
    printf("FATAL ERROR: out of memory: %s\n", strerror(errno));
    exit(EXIT_FAILURE);
  }
  
  // Allocate each row in that column
  for (int i = 0; i < rows; i++) {
    myArray[i] = calloc(cols, sizeof(int));
    if (myArray[i] == NULL) {
      printf("FATAL ERROR: out of memory: %s\n", strerror(errno));
      exit(EXIT_FAILURE);
    }
  }
  
  return myArray;
}

// fill the array with characters
void fillArray(char** myArray, int rows, int cols, struct clue * clues, int numClues, int set) 
{
  int stringLength;
  int count = 1;
  
  /*
  for (int i = 0; i < rows; i++) {
    for (int j = 0; j < cols; j++) {
      myArray[i][j] = '#';
      count++;
    }
  }
  */
  
  if(set == 0)
  {
  	for (int i = 0; i < rows; i++) {
    	for (int j = 0; j < cols; j++) {
     	 myArray[i][j] = '#';
     	 count++;
   		}
  	}
  	for(int k = 0; k < numClues; k++)
  	{
  	// horizontal direction
		if (clues[k].dir == 'H')
		{
		  stringLength = strlen(clues[k].wordAnswer);
		  
		  
		  //printf("(H) Position (%i) or clues[k].startRow = %i clues[k].startCol = %i is %i characters long going %c \n", k, clues[k].startRow, clues[k].startCol, stringLength, clues[k].dir);
		  
		  
		  for (int j = clues[k].startCol; j < clues[k].startCol + stringLength; j++) 
		  {
		    myArray[clues[k].startRow - 1][j - 1] = '_';
		  }
		}
		// vertical direction
		else if (clues[k].dir == 'V')
		{
		  stringLength = strlen(clues[k].wordAnswer);
		  
		  
		  //printf("(V) Position (%i) or clues[k].startRow = %i clues[k].startCol = %i is %i characters long going %c \n", k, clues[k].startRow, clues[k].startCol, stringLength, clues[k].dir);
		  
		  
		  for (int i = clues[k].startRow; i < clues[k].startRow + stringLength; i++) 
		  {
		    myArray[i - 1][clues[k].startCol - 1] = '_';
		  }  
		}
	}
  }
  else if (set == 1)
  {
  	for(int k = 0; k < numClues; k++)
  	{
  	//printf("in fill array: %c end string\n", clues[k].wordAnswer[k]);
	  if(clues[k].found == 1)
	  {
	    int beginString = 0;
	    // horizontal direction
		if (clues[k].dir == 'H')
		{
		  stringLength = strlen(clues[k].wordAnswer);
		  
		  //copy answer into myArray
		  //printf("JAS: %s\n", clues[k].wordAnswer);
		  //printf("JAS: k=%i, startRow=%i\n", k, clues[k].startRow);
		  for (int j = clues[k].startCol; j < clues[k].startCol + stringLength; j++) 
		  {		    
		    //printf("JAS: j=%i\n", j);
		    myArray[clues[k].startRow - 1][j - 1] = clues[k].wordAnswer[beginString];
		    beginString++;
		  }

		}
		// vertical direction
		else if (clues[k].dir == 'V')
		{
		  stringLength = strlen(clues[k].wordAnswer);
		  
		  //copy answer into myArray
		  for (int i = clues[k].startRow; i < clues[k].startRow + stringLength; i++) 
		  {
		    myArray[i - 1][clues[k].startCol - 1] = clues[k].wordAnswer[beginString];
		    beginString++;
		  }  
		}
	  }       
 	 }

  }
  
  
  
  
  
  
  /*
  for(int k = 0; k < numClues; k++)
  {
  //printf("in fill array: %c end string\n", clues[k].wordAnswer[k]);
	  if(clues[k].found == 1)
	  {
	    int beginString = 0;
	    // horizontal direction
		if (clues[k].dir == 'H')
		{
		  stringLength = strlen(clues[k].wordAnswer);
		  
		  //copy answer into myArray
		  //printf("JAS: %s\n", clues[k].wordAnswer);
		  //printf("JAS: k=%i, startRow=%i\n", k, clues[k].startRow);
		  for (int j = clues[k].startCol; j < clues[k].startCol + stringLength; j++) 
		  {		    
		    //printf("JAS: j=%i\n", j);
		    myArray[clues[k].startRow - 1][j - 1] = clues[k].wordAnswer[beginString];
		    beginString++;
		  }
		  break;
		}
		// vertical direction
		else if (clues[k].dir == 'V')
		{
		  stringLength = strlen(clues[k].wordAnswer);
		  
		  //copy answer into myArray
		  for (int i = clues[k].startRow; i < clues[k].startRow + stringLength; i++) 
		  {
		    myArray[i - 1][clues[k].startCol - 1] = clues[k].wordAnswer[beginString];
		    beginString++;
		  }  
		  break;
		}
	  }
	  else if(clues[k].found == 0)
	  {
		// horizontal direction
		if (clues[k].dir == 'H')
		{
		  stringLength = strlen(clues[k].wordAnswer);
		  
		  
		  //printf("(H) Position (%i) or clues[k].startRow = %i clues[k].startCol = %i is %i characters long going %c \n", k, clues[k].startRow, clues[k].startCol, stringLength, clues[k].dir);
		  
		  
		  for (int j = clues[k].startCol; j < clues[k].startCol + stringLength; j++) 
		  {
		    myArray[clues[k].startRow - 1][j - 1] = '_';
		  }
		}
		// vertical direction
		else if (clues[k].dir == 'V')
		{
		  stringLength = strlen(clues[k].wordAnswer);
		  
		  
		  //printf("(V) Position (%i) or clues[k].startRow = %i clues[k].startCol = %i is %i characters long going %c \n", k, clues[k].startRow, clues[k].startCol, stringLength, clues[k].dir);
		  
		  
		  for (int i = clues[k].startRow; i < clues[k].startRow + stringLength; i++) 
		  {
		    myArray[i - 1][clues[k].startCol - 1] = '_';
		  }  
		}
	  }        
  }
  */

  return;
}

void printArray(char** myArray, int rows, int cols) {
  for (int i = 0; i < rows; i++) {
    for (int j = 0; j < cols; j++) {
      printf("%c ", myArray[i][j]);
    }
    printf("\n");
  }
  //return;
}

void deleteArray(char** myArray, int rows) {
  for (int i = 0; i < rows; i++) {
    free(myArray[i]);
  }
  free(myArray);
  
  return;
}


