// Timothy Weatherwax t_weatherwax@u.pacific.edu

#ifndef MEMORY_H
#define MEMORY_H

char** createArray(int rows, int cols);
void fillArray(char** myArray, int rows, int cols, struct clue *clues, int numClues, int set);
void printArray(char** myArray, int rows, int cols);
void deleteArray(char** myArray, int rows);


#endif
